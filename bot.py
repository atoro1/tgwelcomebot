import logging
import os
import sqlite3
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from mwt import MWT

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


class WelcomeBot:
    def __init__(self):
        self.updater = Updater(token=os.environ.get('TG_API_TOKEN'), use_context=True)
        self.dispatcher = self.updater.dispatcher
        self.db = 'db.sqlite3'

    @MWT(timeout=60*60)
    def get_admin_ids(self, bot, chat_id):
        """
        Fetches the Admin members of the chat, caching the results for
        one hour to avoid overload if the bot is in many servers.
        """
        return [admin.user.id for admin in bot.get_chat_administrators(chat_id)]

    @staticmethod
    def db_setup(conn):
        """
        Sets up the local SQLite database on first run, so no special setup is required by the bot owner.
        """
        conn.execute("CREATE TABLE IF NOT EXISTS servers(chat_id INTEGER UNIQUE, welcome TEXT, send_welcome BOOLEAN)")
        conn.commit()

    @staticmethod
    def get_welcome_message(conn, chat_id, update):
        """
        Fetches the welcome message for the chat which fired the event,
        returning False if the chat does not have a welcome message or
        if the send_welcome column is set to False.
        """
        try:
            noob = update.message.new_chat_members[0]
        except IndexError:
            noob = update.effective_user
        welcome_message = conn.execute("SELECT welcome, send_welcome FROM servers WHERE chat_id=?",
                                       (chat_id,)).fetchone()

        if not welcome_message[0]:
            return False
        if not welcome_message[1]:
            return False

        return welcome_message[0].replace('%first_name', noob.first_name if noob.first_name else '')\
            .replace('%last_name', noob.last_name if noob.last_name else '')\
            .replace('%mention', noob.name if noob.name else '')\
            .replace('%chat_name', update.message.chat.title)

    def start(self, update, context):
        """
        Sends a quick snippet of text to the user when they run the /start command
        """
        conn = sqlite3.connect(self.db)
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text="Hi there, I'm the Welcome Bot!\nUse /update_welcome to set a new welcome "
                                      "message.\n\nYou can use variables in your welcome message, such as:\n"
                                      "%first_name - New member's first name\n"
                                      "%last_name - New member's last name\n"
                                      "%mention - Mention's the new member\n"
                                      "%chat_name - Name of the chat, useful if you change names often")
        logging.info(f"({update.effective_chat.title}) Start command ran by {update.effective_user.first_name} "
                     f"{update.effective_user.last_name} ({update.effective_user.mention})")
        conn.close()

    @staticmethod
    def insert_server(conn, update):
        """
        Adds a server to the database, or ignores if server already exists.
        This runs before any server-related database transactions.
        """
        conn.execute("""
            INSERT INTO servers(chat_id, welcome, send_welcome) VALUES (?, ?, ?)
            ON CONFLICT(chat_id) DO NOTHING
        """, (update.effective_chat.id, "Welcome to the server, %mention!", 1))
        conn.commit()

    def toggle_welcome(self, update, context):
        """
        Toggles welcome messages on and off on a per-server basis.
        Requires server admin permissions.
        """
        if update.effective_user.id not in self.get_admin_ids(context.bot, update.message.chat_id):
            return
        conn = sqlite3.connect(self.db)
        self.insert_server(conn, update)
        try:
            self.insert_server(conn, update)
            conn.execute("""
            UPDATE servers SET send_welcome = CASE
                WHEN send_welcome = 1 THEN 0
                ELSE 1
                END
            WHERE chat_id=?
            """, (update.effective_chat.id,))
            conn.commit()
            r = conn.execute("SELECT send_welcome FROM servers WHERE chat_id=?",
                             (update.effective_chat.id,)).fetchone()[0]
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f"Welcome message toggled {'on' if r == 1 else 'off'}!")
        except Exception as e:
            logging.info(str(e))
            pass
        conn.close()

    def update_welcome(self, update, context):
        """
        Updates the welcome message on a per-server basis.
        Requires server admin permissions.
        """
        if update.effective_user.id not in self.get_admin_ids(context.bot, update.message.chat_id):
            return
        conn = sqlite3.connect(self.db)
        conn.execute("INSERT INTO servers(chat_id, welcome, send_welcome) VALUES (?, ?, ?)"
                     "ON CONFLICT(chat_id) DO UPDATE SET welcome=excluded.welcome",
                     (update.effective_chat.id, update.message.text[16:], True))
        conn.commit()
        conn.close()
        context.bot.send_message(chat_id=update.effective_chat.id, text="New welcome message set!")
        logging.info(f"({update.effective_chat.title}) update_welcome command ran by "
                     f"{update.effective_user.first_name} {update.effective_user.last_name} "
                     f"({update.effective_user.mention})")

    def send_welcome(self, update, context):
        """
        Sends the welcome message to a server. Triggered by both the /test_welcome command
        and when a user joins a server.
        """
        if not len(update.message.new_chat_members) > 0:
            return
        conn = sqlite3.connect(self.db)
        self.insert_server(conn, update)
        welcome_message = self.get_welcome_message(conn, update.effective_chat.id, update)
        if not welcome_message:
            return
        context.bot.send_message(chat_id=update.effective_chat.id, text=welcome_message, parse_mode='HTML')
        logging.info(f"{update.effective_user.first_name} {update.effective_user.last_name} "
                     f"joined {update.message.chat.title}")

    def test_welcome(self, update, context):
        """
        Lets users test the welcome message to ensure it's working correctly.
        Requires server admin permissions.
        """
        if update.effective_user.id not in self.get_admin_ids(context.bot, update.message.chat_id):
            return
        conn = sqlite3.connect(self.db)
        self.insert_server(conn, update)
        welcome_message = self.get_welcome_message(conn, update.effective_chat.id, update)
        context.bot.send_message(chat_id=update.effective_chat.id, text=welcome_message, parse_mode='HTML')
        logging.info(f"({update.effective_chat.title}) test welcome command ran by "
                     f"{update.effective_user.first_name} {update.effective_user.last_name} "
                     f" ({update.effective_user.mention})")

    def start_polling(self):
        """
        Adds the command and message handlers and begins polling the servers for updates
        """
        conn = sqlite3.connect(self.db)
        self.db_setup(conn)
        conn.close()
        logging.info("Starting bot...")

        start_handler = CommandHandler('start', self.start)
        self.dispatcher.add_handler(start_handler)
        logging.info("Loaded /start command handler...")

        update_welcome_handler = CommandHandler('update_welcome', self.update_welcome)
        self.dispatcher.add_handler(update_welcome_handler)
        logging.info("Loaded /update_welcome command handler...")

        toggle_welcome_handler = CommandHandler('toggle_welcome', self.toggle_welcome)
        self.dispatcher.add_handler(toggle_welcome_handler)
        logging.info("Loaded /toggle_welcome command handler...")

        test_welcome_handler = CommandHandler('test_welcome', self.test_welcome)
        self.dispatcher.add_handler(test_welcome_handler)
        logging.info("Loaded /test_welcome command handler...")

        send_welcome_handler = MessageHandler(Filters.status_update.new_chat_members, self.send_welcome)
        self.dispatcher.add_handler(send_welcome_handler)
        logging.info("Loaded message handler for new members...")

        self.updater.start_polling()
        logging.info("Handlers loaded")

    def idle(self):
        """
        Sets the bot to idle to save resources while waiting for updates
        """
        logging.info("Going idle...")
        self.updater.idle()


bot = WelcomeBot()

bot.start_polling()
bot.idle()
