# TG Welcome Bot

## Installation

1. Copy this repo to your machine
2. Talk to BotFather on Telegram to make your new bot and get an API token
3. Copy the API token you get and save it somewhere

## Setup

1. Add an environmental variable with your token named 'TG_API_TOKEN'
    * On Ubuntu or Debian for example: `export TG_API_TOKEN=abcd1234`
2. Make sure you have the latest Python version. This project was created with Python 3.8+ in mind
3. Create a virtual environment for Python in your bot folder
    * `python3.8 -m venv botvenv`
4. Load into your venv and install the requirements
    * `source botvenv/bin/activate`
    * `pip install -r requirements.txt`

## Usage

1. Open up a TMUX window or your preferred alternative
2. Load into your venv again
3. Run the bot and watch the log to make sure it worked
    * `python bot.py`
    * If successful, the last thing you should see is 'Going idle...'
4. Invite your bot to your server, add a welcome message, and enjoy!

## Commands

* /start
    * Shows some basic info for the bot
* /update_welcome
    * Updates your server's welcome message to whatever text follows the command. Variables you can use are:
        * %first_name - New member's first name
        * %last_name - New member's last name
        * %mention - Mention's the newmember
        * %chat_name - The name of the chat, useful if you change names often
* /test_welcome
    * Tests the welcome message on you, so you can be sure it looks how you want it
* /toggle_welcome
    * Toggles the welcome message on and off
